#include "nRF24L01.h"

//
// SPI functions
//

//#define ATMEGA
// For the ATTINY MISO and MOSI must be crossed over to communicate with the RF24. Thus,
// programming and operating has two different configurations that must be manually set on
// the breadboard programmer.
#define ATTINY

void set_csn(uint8_t high) {
    if(high)
        PORTB |= (1 << PB2);
    else
        PORTB &= ~(1 << PB2);
}

void set_ce(uint8_t high) {
    if(high)
        PORTB |= (1 << PB1);
    else
        PORTB &= ~(1 << PB1);
}

void spi_init() {
    #ifdef ATMEGA
    // set SCK, MOSI, SS/CSN, CE (PB1)  as output
    DDRB |= (1<<DDB5) | (1<<DDB3) | (1<<DDB2) | (1<<DDB1);
    
    // enable SPI and set clock rate to fck/16
    SPCR |= (1<<SPE) | (1<<MSTR);
    #else
    //Connect MISO from RF24 to MOSI!! on muc (and vice versa)
    // SCK (PA4), CSN (PB2), CE (PB1)
    DDRA |= (1<<PA4) | (1<<PA5);
    DDRB |= (1<<PB2) | (1<<PB1);

    USICR |= (1<<USIWM0) | (1<<USICS1) | (1<<USICLK);
    #endif

    set_csn(1);
    set_ce(0);
}

uint8_t spi_write_byte(uint8_t data) {
    #ifdef ATMEGA
    // write for data register
    SPDR = data;

    // wait for transmission to complete
    while( !(SPSR & (1<<SPIF)) ) ; 

    return SPDR; // result stored in SPDR
    #else
    USIDR = data;
    USISR |= (1<<USIOIF);

    while( (USISR &  (1<<USIOIF)) == 0)
        USICR |= (1<<USITC);

    return USIDR;
    #endif
}


//
// functions for the RF24
//

void rf_write(uint8_t reg, uint8_t num, uint8_t* data) {
    // TODO: separate the register writes into another function
    if(reg != W_TX_PAYLOAD && reg != FLUSH_TX && reg != FLUSH_RX && reg != ACTIVATE)
        reg = W_REGISTER + reg;

    _delay_us(10);
    set_csn(0);
    _delay_us(10);
    spi_write_byte(reg);
    _delay_us(10);

    for(int i=0;i<num;i++) {
        spi_write_byte(data[i]);
        _delay_us(10);
    }

    set_csn(1);
}

void rf_read(uint8_t reg, uint8_t num, uint8_t* data) {
    _delay_us(10);
    set_csn(0);
    _delay_us(10);
    spi_write_byte(reg);
    _delay_us(10);

    for(int i=0;i<num;i++) {
        data[i] = spi_write_byte(NOP);
        _delay_us(10);
    }

    set_csn(1);
}

void rf_write_register(uint8_t reg, uint8_t data) {
    rf_write(reg, 1, &data);
}

uint8_t rf_read_register(uint8_t reg) {
    uint8_t data;
    rf_read(reg, 1, &data);
    return data;
}

uint8_t rf_get_register(uint8_t reg) {
    _delay_us(10);
    set_csn(0);
    _delay_us(10);
    spi_write_byte(R_REGISTER+reg);
    _delay_us(10);
    uint8_t ret = spi_write_byte(NOP); // dummy byte
    _delay_us(10);
    set_csn(1);
    return ret;
}

enum rf_init_status {
    rf_init_success, rf_init_error
};

enum rf_init_status rf_init(char addr[5]) {

    enum rf_init_status ret = rf_init_success;

    _delay_ms(500);
    spi_init();
    _delay_ms(500);

    // crc
    rf_write_register(CONFIG, 0x0C);

    // retries
    uint8_t delay = 5, count = 15;
    rf_write_register(SETUP_RETR, (delay&0xf)<<ARD | (count&0xf)<<ARC);

    // set data rate to 1MB/s
    uint8_t setup = rf_read_register(RF_SETUP);
    setup &= ~(_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));
    rf_write_register(RF_SETUP,setup);

    // toggle
    rf_write_register(ACTIVATE, 0x73);

    // disable dynamic payload
    rf_write_register(FEATURE, 0);
    rf_write_register(DYNPD, 0);

    // reset current status
    rf_write_register(STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));

    // set channel
    rf_write_register(RF_CH, 76);

    // flush
    rf_write(FLUSH_TX, 0, NULL);
    rf_write(FLUSH_RX, 0, NULL);

    // power up
    uint8_t cfg = rf_read_register(CONFIG);
    if(!(cfg & _BV(PWR_UP))) {
        // if the radio is not powered up we do that now
        rf_write_register(CONFIG, cfg | _BV(PWR_UP));

        _delay_ms(5);
    }

    rf_write_register(CONFIG, rf_read_register(CONFIG) & ~_BV(PRIM_RX) );

    // open writing pipe
    rf_write(RX_ADDR_P0, 5, (uint8_t*)addr);
    rf_write(TX_ADDR, 5, (uint8_t*)addr);
    rf_write_register(RX_PW_P0, 32);

    return ret;
}

enum rf_send_status {
    rf_send_success, rf_send_error_timeout, rf_send_error_status
};

enum rf_send_status rf_send(void* data, size_t num) {
    enum rf_send_status ret = rf_send_success;

    rf_write(FLUSH_TX, 0, NULL);

    uint8_t c[32] = "XXX\n";
    memcpy(c, data, num);
    rf_write(W_TX_PAYLOAD, 32, c);

    set_ce(1);
    _delay_us(10);
    set_ce(0);

    uint8_t cnt=0;
    while( !(rf_get_register(STATUS) & ( _BV(TX_DS) | _BV(MAX_RT) )) ) {
        _delay_us(1000);
        cnt++;

        if(cnt > 95) {
            ret = rf_send_error_timeout;
            break;
        }
    }

    set_ce(0);
    uint8_t status = rf_get_register(STATUS);
    if( status & _BV(MAX_RT) ) {
        rf_write(FLUSH_TX, 0, NULL);
        // TODO: this indicates an error condition but in practice
        // everything works perfectly, i.e. the package is delivered
        // without any issues.
        //if(ret == rf_send_success)
        //    ret = rf_send_error_status;
    }

    return ret;
}
