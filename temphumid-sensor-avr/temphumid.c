#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/power.h>

#include "rf24.h"
#include "DHT.h"


const uint8_t id = 7;


#ifdef ATMEGA // ATMEGA328P
#define port_output PORTD
#define pin_output  PD1
#else //ATTINY44
#define port_output PORTA
#define pin_output  PA0
#endif

void blink_rapidly(int num) {
    for(int i=0;i<num;i++) {
        port_output |= (1 << pin_output);
        _delay_ms(200);
        port_output &= ~(1 << pin_output);
        _delay_ms(200);
    }
}

struct payload {
    uint8_t id;
    float t;
    float h;
};

ISR(WDT_vect) {
    wdt_reset();
}

int main() {

    // disable the watchdog
    MCUSR = 0;   // needed otherwise we get a reset loop
    wdt_disable();

    #ifdef ATMEGA
    DDRD |= (1<<DDD1);
    #else
    DDRA |= (1<<DDA0);
    #endif
    //blink_rapidly(2);


    if(rf_init("MHASN") != rf_init_success)
        blink_rapidly(10);

    DHT_Setup();

    while(1) {

        double temp, hum;
        DHT_Read(&temp, &hum);
        struct payload data;
        data.id = id;
        data.t  = temp;
        data.h  = hum;

        enum DHT_Status_t s = DHT_status();
        if(s == DHT_Error_Checksum)
            blink_rapidly(11);
        else if(s == DHT_Error_Timeout)
            blink_rapidly(12);
        else {
            // send the data
            enum rf_send_status rfs = rf_send(&data, sizeof(struct payload));
            if(rfs == rf_send_error_timeout)
                blink_rapidly(7);
            else if(rfs == rf_send_error_status)
                blink_rapidly(9);
            else if(rfs != rf_send_success)
                blink_rapidly(5);
        }


        // being idle on attiny44 is about 0.9 mA
        // entering idle sleep mode is about 0.5 mA
        // entering power down sleep mode is about 0.12 maA
        //set_sleep_mode(SLEEP_MODE_IDLE);

        // sleep for 64 seconds
        for(uint8_t i=0;i<8;i++) {
            // switch watchdog timer to interrupt 
            cli(); // disable interrupts
            MCUSR=0;
            WDTCSR |= 0b00011000;
            WDTCSR  = 0b111001;
            wdt_enable(WDTO_8S);
            WDTCSR |= (1<<WDIE);
            sei(); // enable interrupts

            //set_sleep_mode(SLEEP_MODE_IDLE);
            set_sleep_mode(SLEEP_MODE_PWR_DOWN);
            sleep_mode();

            sleep_disable();
        }
        
        wdt_enable(WDTO_1S);
        _delay_ms(5.0*1000);

/*
        _delay_ms(60.0*1000);

        // resets the micro in 1s
        wdt_enable(WDTO_1S);

        _delay_ms(5.0*1000);
        */
    }
}

