The server tries to listen on port 80. In most Linux systems a normal user is
not allowed to do that. The utility authbind can be used to change this behavior.

sudo apt-get install authbind
sudo touch /etc/authbind/byport/80
sudo chmod 777 /etc/authbind/byport/80
Then run the server using the Makefile (which includes an authbind command)

