# only python2

from SimpleHTTPServer import SimpleHTTPRequestHandler
from SocketServer import TCPServer as HTTPServer

server_port = 8080
try:
    server_port = int(sys.argv[1])
except:
    pass

def extension(fn):
    import os
    return os.path.splitext(fn)[1]

class MyRequesthandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        print(self.path)
        if self.path == '/':
            self.path = '/index.html'
        try:
            with open('.' + self.path, 'r') as content_file:
                content = content_file.read()
                self.send_response(200)
                if extension(self.path)=='.png':
                    self.send_header("Content-type", "image/png")
                elif extension(self.path)=='.svg':
                    print(self.path)
                    self.send_header("Content-type", "image/svg+xml")
                else:
                    self.send_header("Content-type", "text/html")
                self.send_header("Content-length", len(content))
                self.end_headers()
                self.wfile.write(content)
        except Exception as e:
            print('could not serve: ', self.path)
            print(e)

httpd = HTTPServer(("", server_port), MyRequesthandler)
print("serving at port {0}".format(server_port))
httpd.serve_forever()

