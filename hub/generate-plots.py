#!/usr/bin/env python3
from numpy import *
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
from matplotlib.dates import *
from datetime import datetime, timedelta
import time
import sqlite3
import json
import re


db = sqlite3.connect('../hub-data/sensor-data.db')

def dewpoint(T, H):
    a = 6.112; b = 17.67; c = 243.5;
    gamma = log(H/100.0)+b*T/(c+T)
    return c*gamma/(b-gamma)

def load_data(descr, delta_t, units_of_sec=False):
    ts = time.time() - delta_t

    sql_query = 'select timestamp,temperature,humidity FROM temphumid WHERE description = "{}" and timestamp >= {}'
    cursor = db.cursor()
    cursor.execute(sql_query.format(descr,ts))
    db_data = cursor.fetchall()
    cursor.close()

    if units_of_sec==True:
        times  = [a[0] for a in db_data] 
    else:
        times  = [datetime.fromtimestamp(a[0]) for a in db_data] 
    temps  = [a[1] for a in db_data]
    humids = [a[2] for a in db_data]

    return (times,temps,humids)

def load_config_temphumid():
    jd = json.loads(open('../hub-data/config.json').read())['temphumid']
    keys=[]
    for k in sorted(jd):
        keys.append(k)
    return keys, jd

def generate(name, delta_t):
    keys, jd = load_config_temphumid()

    fig=figure(figsize=(15,5))

    ax1 = subplot(1,2,1)
    ylabel('Temperature [C]')
    for idx in keys:
        descr=jd[idx]
        times,temps,humids = load_data(descr, delta_t)
        if len(times) >= 1:
            plot(times,temps,label='{}\n{}C {}%'.format(descr,temps[-1],humids[-1]),linewidth=3)
    ax1.xaxis_date()
    xlim(ax1.xaxis.get_data_interval())
    grid()

    # legend below the axis
    x=1.1
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
    ax1.legend(loc='upper center', bbox_to_anchor=(x, 1.16),
            fancybox=True, shadow=True, ncol=5)

    ax1 = subplot(1,2,2)
    ylabel('Rel. Humidity [%]')
    for idx in keys:
        descr=jd[idx]
        times,_,humids = load_data(descr, delta_t)
        if len(times) >= 1:
            plot(times,humids,label=descr,linewidth=3)
    ax1.xaxis_date()
    xlim(ax1.xaxis.get_data_interval())
    grid()

    # legend below the axis
    #box = ax1.get_position()
    #ax1.set_position([box.x0, box.y0 + box.height * 0.1,
    #             box.width, box.height * 0.9])
    #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),
    #        fancybox=True, shadow=True, ncol=5)

    fig.autofmt_xdate()
    fig.savefig('{}.png'.format(name))

def generate_dewpoint(name, l_descr, delta_t):
    fig=figure(figsize=(7.5,5))

    ax1 = subplot(1,1,1)
    ylabel('Temperature [C]')
    for descr in l_descr:
        times,temps,humids = load_data(descr, delta_t)
        #dew_minus_T = 0.2*(100.0-array(humids)) # approximate formula
        dew_minus_T = temps - dewpoint(array(temps),array(humids))
        plot(times,dew_minus_T,label='{}'.format(descr),linewidth=3)
    ax1.xaxis_date()
    xlim(ax1.xaxis.get_data_interval())
    grid()

    # legend below the axis
    x=0.5
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
    ax1.legend(loc='upper center', bbox_to_anchor=(x, -0.15),
            fancybox=True, shadow=True, ncol=5)

    fig.autofmt_xdate()
    fig.savefig('{}.png'.format(name))

def generate_floorplan(fn):
    with open(fn) as fs:
        content = fs.read()

    keys, jd = load_config_temphumid()
    for idx in keys:
        descr=jd[idx]
        times,temps,humids = load_data(descr, 60*60)
        try:
            content = content.replace('__{}_T_'.format(descr), '{:.1f} C'.format(temps[-1]))
            content = content.replace('__{}_H_'.format(descr), '{:.0f} %'.format(humids[-1]))
            dew_diff = temps[-1]-dewpoint(temps[-1],humids[-1])
            dew_str  = 'D+{:.1f} C'.format(dew_diff) if dew_diff>=0 else 'D{:.1f} C'.format(dew_diff)
            content = content.replace('__{}_D_'.format(descr), '{}'.format(dew_str))
        except IndexError:
            pass

    with open('floorplan-out.svg', 'w') as fs:
        fs.write(content)


def generate_ui(fn):
    with open(fn) as fs:
        content = fs.read()

    keys, jd = load_config_temphumid()
    for idx in keys:
        descr=jd[idx]
        times,temps,humids = load_data(descr, 60*60)
        try:
            print(descr, temps[-1], humids[-1])
            content = content.replace('__{}_T_'.format(descr), '{:.1f} C'.format(temps[-1]))
            content = content.replace('__{}_H_'.format(descr), '{:.0f} %'.format(humids[-1]))
            dew_diff = temps[-1]-dewpoint(temps[-1],humids[-1])
            dew_str  = 'D+{:.1f}'.format(dew_diff) if dew_diff>=0 else 'D{:.1f}'.format(dew_diff)
            content = content.replace('__{}_D_'.format(descr), '{}'.format(dew_str))
        except IndexError:
            pass

    content = re.sub(r'__(.*)_', 'NA', content)

    with open('index.html', 'w') as fs:
        fs.write(content)



def window_open_info():
    f = open("window_open.html", "wt")
    f.write('<font size="+2">')

    keys, jd = load_config_temphumid()
    for id in keys:
        descr = jd[id]
        times, temps, humids = load_data(descr, 8*60.0, units_of_sec=True)

        if len(temps) < 2:
            print('ERROR: window_open_info has not enough data for {}'.format(descr))
            continue

        slope_temp = polyfit(times, temps, 1)[0]
        slope_humid = polyfit(times, humids, 1)[0]
        print(descr, slope_temp, slope_humid, temps, humids)
        # change more than 0.075 degrees per minute
        if slope_temp <=  -0.02/60.0:
            f.write('- <b>{} window is open<b/> -'.format(descr))

    f.write('</font>')
    f.write('<br/>\n')
    f.close()


def generate_multiplot(delta_t):
    keys, jd = load_config_temphumid()

    descrs = ['living_room', 'kitchen', 'corridor_downstairs', 'lab', 'martina', 'bedroom', 'bath', 'basement', 'basement2', 'basement3']

    now = datetime.fromtimestamp(time.time())
    tt = now.replace(second=0,minute=0)
    tt_list = [tt -timedelta(hours=i*12) for i in range(0,6)]

    # import matplotlib.pyplot as plt
    # plt.style.use('dark_background')
    fig=figure(figsize=(10,1.1*len(descrs)))

    plot_id=0
    for descr in descrs:
        plot_id += 1
        ax1 = subplot(len(descrs),1,plot_id)

        times,temps,humids = load_data(descr, delta_t)
        if len(times) >= 1:
            if descr == 'bath' or descr == 'basement' or descr == 'basement2' or descr == 'basement3':
                title('{}: {}C {}% D+{:.1f}'.format(descr,temps[-1],humids[-1],temps[-1]-dewpoint(temps[-1],humids[-1])), fontweight='bold')
            else:
                title('{}: {}C {}%'.format(descr,temps[-1],humids[-1]), fontweight='bold')
            plot(times,temps,linewidth=2,color="tab:red")
            if descr == 'bath' or descr == 'basement' or descr == 'basement2' or descr == 'basement3':
                dew_minus_T = temps - dewpoint(array(temps),array(humids))
                dew_minus_T += 17.5 if descr=='bath' else 10
                plot(times,dew_minus_T,linewidth=2,color='tab:brown')

            xlim(ax1.xaxis.get_data_interval())
            grid()
            if descr != 'basement' and descr != 'basement2' and descr != 'basement3':
                ylim([17.5,25])
                ax1.set_yticks([17.5,20,22.5,25])
                ax1.set_yticklabels(['17.5','20','22.5','25'])
            else:
                ylim([10,20])
                ax1.set_yticks([10,13.3,16.6,20])
                ax1.set_yticklabels(['10','13.3','16.6','20'])
            
            #if descr=='new':
            #    ax1.set_xticklabels([])
            #else:
            #    ax1.xaxis_date()
            #    fig.autofmt_xdate()

        ax2 = ax1.twinx()
        ax2.plot(times,humids,linewidth=2,color="tab:blue")
        ax2.set_ylim([25,100])
        ax2.set_yticks([25,50,75,100])
        ax2.set_yticklabels(['25','50','75','100'])
        ax1.set_xticks(tt_list)
        ax1.set_xticklabels([])

        ax1.set_xticklabels([])

    ax1.set_xticks(tt_list)
    ax1.set_xticklabels([x.hour for x in tt_list])
    tight_layout()
    fig.savefig('multiplot.png', bbox_inches='tight', pad_inches=0.1)



start = time.time()
##window_open_info()
#generate_multiplot(2.0*24*3600.0)
#generate_floorplan('floorplan.svg')
generate_ui('index-in.html')
#generate('lastday', 7.0*24.0*3600.0) # go back one week
#generate('lasthour', 24.0*3600.0) # go back one day
#generate_dewpoint('dewpoint', ['basement', 'bath'], 4.0*24.0*3600.0) # dew point
print('plot generation took: ', time.time()-start)

