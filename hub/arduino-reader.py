import serial
import datetime
from time import sleep
import time
import sqlite3
import json
import sys

ser = serial.Serial('/dev/ttyACM0',9600,timeout=0.01)

# read whatever is still in the buffer
while ser.readline():
    pass

sql_insert_statement='insert into temphumid (timestamp,description,temperature,humidity) values ({},"{}",{},{})'
db = sqlite3.connect('../hub-data/sensor-data.db')

while True:
    data = ser.readline()
    if data:
        txt = data.decode("utf-8")
        print('received: ', txt.strip('\n'))
        if txt[0] != 'S':
            try:
                ts = time.time()
                sensor_id,temp,humid = txt.strip('\n').split(' ')
                temp  = float(temp)
                humid = float(humid)

                jd = json.loads(open('../hub-data/config.json').read())
                descr = jd['temphumid'][sensor_id]

                try:
                    humid += float(jd['offset'][sensor_id])
                except KeyError:
                    pass

                cursor = db.cursor()
                cursor.execute(sql_insert_statement.format(ts,descr,round(temp,2),round(humid,2)))
                db.commit()
                cursor.close()
            except:
                print("Unexpected error:", sys.exc_info()[0])
    else:
        sleep(0.1)

ser.close()

