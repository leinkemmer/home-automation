/* sqlite sensor-data.db < ../hub/new-db.sql */
create table temphumid ( 
    id          integer     not null primary key,
    timestamp   datetime    not null,
    description varchar(50) not null,
    temperature float       not null, 
    humidity    float       not null
);

