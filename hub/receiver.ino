#include <SPI.h>
#include "RF24.h"

RF24 radio(7,8);
byte addr[6] = {"MHASN"}; // id

void setup() {
    // serial
    Serial.begin(9600);
    Serial.println("STATUS: gateway initialization.");

    // radio
    radio.begin();
    radio.setPALevel(RF24_PA_MIN);

    radio.openReadingPipe(1,addr);
    radio.startListening();
}


struct payload {
    byte id;
    float t;
    float h;
};

void loop() {

    if(radio.available()) {
        payload pl;
        radio.read(&pl, sizeof(payload));
        Serial.print(pl.id);
        Serial.print(" ");
        Serial.print(pl.t);
        Serial.print(" ");
        Serial.println(pl.h);
    } else {
        Serial.println("STATUS: radio not available");
    }

    delay(1000);
}

