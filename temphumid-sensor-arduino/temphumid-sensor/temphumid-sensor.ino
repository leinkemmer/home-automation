#include <Adafruit_Sensor.h>
#include "DHT.h"

#include <SPI.h>
#include "RF24.h"

#include <avr/wdt.h>

const byte sensor_id = 2;

//RF24 radio(7,8); // breadboard
RF24 radio(7,4);  // soldered
byte addr[6] = {"MHASN"}; // id

DHT dht(5, DHT22);

void setup() {
    MCUSR = 0;   // needed otherwise we get a reset loop
    wdt_disable();

    // serial
    Serial.begin(9600);
    Serial.println("temperatur-radio initialization.");

    // radio
    radio.begin();
    radio.openWritingPipe(addr);
    radio.setPALevel(RF24_PA_MAX);
    radio.stopListening();

    // sensor
    dht.begin();
}


struct payload {
    byte id;
    float t;
    float h;
};

void loop() {
    // sensor
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println(" *C ");

    // radio
    payload pl;
    pl.id = sensor_id;
    pl.t  = t;
    pl.h  = h;
    if (!radio.write( &pl, sizeof(pl) )){
        Serial.println(F("send failed"));
    }


    // wait a minute
    for(int i=0;i<60;i++)
        delay(1000);

    // reset after 1s
    wdt_enable(WDTO_1S);

    delay(5000);
}

