Software in support of various home automation endeavours. At the moment this
consists primarily of a temperature/humidity sensor (for the Arduino platform
and the ATmega328P and ATtiny44 AVR microcontrollers) and the corresponding
Linux software to receive the data, store them in a database, plot them, and
serve the resulting plots to a web browser.  
